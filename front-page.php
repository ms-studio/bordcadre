<?php
/**
 * The front page template file.
 *
 * @package Promenade
 * @since 1.0.0
 */

get_header();

?>

<main id="primary" class="content-area front-page" role="main" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/Blog">

	<?php while ( have_posts() ) : the_post();
	
	// Get  the language taxonomy of current post
	
	// Set English as default:
	$bcf_current_lang = "en";
	
	// Test if French
	if ( has_term( 'fr', 'lang' ) ) {
	
		$bcf_current_lang = "fr";
	
	}
	
	 ?>
	<?php endwhile;
	
	
	/*
	
	 contents of front page:
	 
	 1) featured news (headline)
	 
	 */
	 
	 $exclude_id = array();
	 
	 $custom_query = new WP_Query( array(
	   					 	'posts_per_page' => 1,
	   					 	'post_type' => 'news',
	   					 	'tax_query' => array(
	   					 			array(
	   					 				'taxonomy' => 'news-type',
	   					 				'field'    => 'slug',
	   					 				'terms'    => 'headline',
	   					 			),
	   					 		),
	   					 		'orderby' => 'date',
	   					 		'order' => 'DESC',
	   					 	) ); 
	   					 	
	   		
	   		/*
	   		 * Option:
	   		 
	   		 'tax_query' => array(
	   		 			'relation' => 'AND',
	   		 			array(
	   		 				'taxonomy' => 'news-type',
	   		 				'field'    => 'slug',
	   		 				'terms'    => 'headline',
	   		 			),
	   		 			array(
	   		 				'taxonomy' => 'lang',
	   		 				'field'    => 'slug',
	   		 				'terms'    => $bcf_current_lang,
	   		 			),
	   		 		),
	   		 		
	   		*/

	   					 	if ($custom_query->have_posts()) : 
	   					 
	   		  			 	?>
	   		  			 	<section class="front-headline">
	   		  			 	<?php
	   		  			 	         
	   		  			 while( $custom_query->have_posts() ) : $custom_query->the_post();
	   		  			 		   		  			
		   		  				$current_post_id = get_the_ID();
		   		  				$exclude_id[] = $current_post_id;
		   		  			
		   		  			if ( $thumbnail_id = get_post_thumbnail_id() ) {

										$image_attributes = wp_get_attachment_image_src( $thumbnail_id, 'large' );				
		   		  				
		   		  				?><figure class="hero-image" style="background-image: url(<?php 
		   		  				
		   		  				echo $image_attributes[0];
		   		  				
		   		  				 ?>);">
		   		  					
		   		  					<?php 
		   		  			} else {
		   		  			
		   		  					echo '<figure class="hero-no-image">';
		   		  			
		   		  			}
		   		  					
		   		  					the_title( '<h1 class="entry-title" itemprop="headline"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" itemprop="url">', '</a></h1>' ); ?>
		   		  					
		   		  				<?php
		   		  				
		   		  			?><div class="entry-content">
		   		  					
		   		  					<div class="entry-summary">
		   		  						<?php the_content(); ?>
		   		  					</div>
		   		  				</div>
		   		  				
		   		  				</figure><?php
	   		  			 
	   		  			 endwhile; 
	   		  			 ?></section><?php
	   					endif;
	   			wp_reset_postdata();
	 
	 /*
	 
	 2) current news (actualités : 5 of them )
	 
	 */
	 $custom_query = new WP_Query( array(
	   					 	'post_type' => 'news',
	   					 	'posts_per_page' => 5,
	   					 	'post__not_in' => $exclude_id ,
	   					 		'orderby' => 'date',
	   					 		'order' => 'DESC',
	   					 	) ); 
	   					 	
	  if ($custom_query->have_posts()) : 
	  		?>
	  			<section class="front-news">
	  			<?php
	 			while( $custom_query->have_posts() ) : $custom_query->the_post();
	 					
	 					the_title( '<h3 class="h3 entry-title">', '</h3>');
	 					echo '<div class="news-date">'.get_the_time("j F Y").'</div>';
	 					echo '<div class="content">';
	 					the_content();
	 					echo '</div>';
	 					// the_excerpt();
	 					
	 			endwhile; 
	 			?></section><?php
	 endif;
	 wp_reset_postdata();
	 
	 /*
	 3) all the films, by chronology
	 
	 */
	 
	 $bcf_lang_var = bcf_lang_tax();
	 	 
	 $custom_query = new WP_Query( array(
	   					 	'post_type' => 'post',
	   					 	'posts_per_page' => -1,
	   					 	'orderby' => 'date',
	   					 	'order' => 'DESC',
	   					 	'tax_query' => array(
	   					 				array(
	   					 					'taxonomy' => 'lang',
	   					 					'field'    => 'slug',
	   					 					'terms'    => $bcf_lang_var,
	   					 				),
	   					 			),
	   					 	) ); 
	   					 	
	  if ($custom_query->have_posts()) : 
			  ?>
			  	<section class="front-films">
			  	<?php
	 			while( $custom_query->have_posts() ) : $custom_query->the_post();
	 					
	 					echo '<div class="film-item">';
	 					echo '<a class="film-link" href="'.get_permalink().'">';
	 					// show image
	 					if ( $thumbnail_id = get_post_thumbnail_id() ) {
	 							
	 							echo '<div class="film-thumbnail">';
	 							the_post_thumbnail( 'medium' );
	 							echo '</div>';
	 					
	 					} else {
	 					
	 					echo '<div class="film-thumbnail">';
	 					echo '<img width="213" height="300" src="'.get_stylesheet_directory_uri().'/img/coming-soon-200px.jpg" class="attachment-medium wp-post-image" />';
	 					echo '</div>';
	 					}
	 					
	 					the_title( '<h3 class="h3 entry-title">', '</h3>');
	 					
	 					echo '</a>';
	 					echo '</div>';
	 					
	 			endwhile; 
	 			?></section><?php
	 endif;
	 wp_reset_postdata();

	 ?>

</main>

<?php
get_footer();

<?php


/* Front Page Query
*********************/

function front_page_query($query) {

  if ( !is_admin() && $query->is_main_query() ) {
  
   
 		// also modify query for archive pages
 		
 		if ( is_archive() ) {
 		
 				set_query_var('post_type', array('posts','news','directors') );
 		
 		}
 
  }
  
  
  
}

add_action('pre_get_posts','front_page_query');
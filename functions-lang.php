<?php


// NOTE: this file is included in header.php

// test if we are a Film or Realisateur

if ( is_singular() ) {

		$current_posttype = get_post_type();
	 	$translated_posttypes = array("post", "page", "director");
 
 if (in_array( $current_posttype, $translated_posttypes)) {
			
     // do stuff
     
     
     // 1. test if there's a p2p connection.
     
     // $current_post_id = get_the_ID();
     
     $connected_stuff = new WP_Query( array(
        'posts_per_page' => -1,
        'connected_type' => 'bcf_p2p_trad',
        'nopaging' => true,
        'connected_items' => get_the_ID(), 
        // get_queried_object_id(), // $this_post_id
        'order' => 'DESC', // desc = newest first
      ) );
      
      // Display connected PROJECTS
      if ( $connected_stuff->have_posts() ) :
           
      	     while ( $connected_stuff->have_posts() ) : $connected_stuff->the_post(); 
      	     
      	     // test if english or french
      	     $bcf_trad_lang_var = "fr";
      	     
      	     if ( has_term( 'en', 'lang' ) ) {
      	     	$bcf_trad_lang_var = "en";
      	     }
      	     
      	      ?><li class="menu-item menu-item-type-post_type menu-item-object-post"><a href="<?php the_permalink(); ?>"><?php 
      	      
      	      if ( $bcf_trad_lang_var == "en" ) {
      	      
      	      	echo 'EN';
      	      } else {
      	      	echo 'FR';
      	      }
      	      
      	       ?></a> 
      	      </li> <?php 
      	      
      	     endwhile;
          
      // Prevent weirdness
      wp_reset_postdata();
      endif;
      // end stuff
     
     
 }

}


   

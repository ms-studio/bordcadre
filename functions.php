<?php 

// Change-Detector-XXXXXXXXX - for Espresso.app

/* Allow Automatic Updates
 ******************************
 * http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */

add_filter( 'auto_update_plugin', '__return_true' );
//add_filter( 'auto_update_theme', '__return_true' );
//add_filter( 'allow_major_auto_core_updates', '__return_true' );


add_action( 'after_setup_theme', 'my_child_theme_setup' );
function my_child_theme_setup() {
    
//    load_theme_textdomain( 'organicthemes', get_stylesheet_directory() . '/lang' );

			load_theme_textdomain( 'promenade', get_stylesheet_directory() . '/languages' );

		/*
		 * Custom Sizes
		 **************************
		*/
    
    // note: thumbnails in MOKA:
    // add_image_size( 'recentposts-widget-img', 300, 300, true );
    // add_image_size( 'recent-img', 360, 480, true );
    // add_image_size( 'slider-img', 1070, 600, true );
//    add_image_size( 'poster-img', 300, 425, true );

		
		register_nav_menus( array(
			'primary_en' => __( 'Primary Menu English', 'promenade' ),
		) );

    
}


function remove_menus(){
  
//  remove_menu_page( 'edit.php?post_type=team' ); //Team
  remove_menu_page( 'edit-comments.php' ); //Comments
  
}
add_action( 'admin_menu', 'remove_menus' );


/* Styles and Scripts
******************************/

function bcf_register_fonts() {
	$protocol = is_ssl() ? 'https' : 'http';
	
	// https://www.google.com/fonts/specimen/Lato
	wp_register_style( 'bcf-webfonts', "$protocol://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" );
	// Lato has: 100, 300, 400, 700, 900
	// and italics

}
add_action( 'init', 'bcf_register_fonts' );


function custom_register_styles() {

	/**
	 * Custom CSS
	 */
	 
	$bordcadre_dev_mode = false;
	
	if ( current_user_can('edit_others_pages') ) {
		 $bordcadre_dev_mode = true;
	}
	
	if ( WP_DEBUG == true ) {
		$bordcadre_dev_mode = true;
	}
	
	 
	 if ( $bordcadre_dev_mode == true ) {
	 
	 		// DEV: the MAIN stylesheet - uncompressed
	 		
	 		wp_enqueue_style( 
	 				'main-style', 
	 				get_stylesheet_directory_uri() . '/css/dev/00-main.css', // main.css
	 				false, // dependencies
	 				'2019.04.24' // version
	 		); 
	 
	 } else {
	 
	 		// PROD: the MAIN stylesheet - combined and minified
	 		wp_enqueue_style( 
	 				'main-style', 
	 				get_stylesheet_directory_uri() . '/css/build/styles.20190424143346.css', // main.css
	 				false, // dependencies
	 				null // version
	 		); 
	 }

		
		wp_dequeue_style( 'promenade-fonts' );
		wp_deregister_style( 'promenade-fonts' );
		
		wp_enqueue_style( 'bcf-webfonts' );
		
	wp_enqueue_script( 
	// the MAIN JavaScript file -- development version
			'main-script',
			get_stylesheet_directory_uri() . '/js/scripts.js', // scripts.js
			array('jquery'), // dependencies
			null, // version
			true // in footer
	);

}
add_action( 'wp_enqueue_scripts', 'custom_register_styles', 25 );


/* excerpt / read more link
******************************/

//add_filter( 'the_content_more_link', 'modify_read_more_link' );
//function modify_read_more_link() {
//	return '<a class="more-link" href="' . get_permalink() . '">Your Read More Link Text</a>';
//}

// Prevent Page Scroll When Clicking the More Link

function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );


/* add stuff to header
******************************/

add_action('wp_head','bcf_header_code');

function bcf_header_code()
{

$output='<link rel="shortcut icon" href="'.esc_url( home_url() ).'/favicon.ico?19328131" />';

echo $output;

}

/* query modifications
******************************/

 require_once('functions-pre-get.php');

/* custom body class
***********/
 
 function bcf_body_class( $classes ) {
 	if ( is_page_template( 'templates/front.php' ) ) {
 		$classes[] = 'front-page';
 	}
 	
 	return array_unique( $classes );
 }
 add_filter( 'body_class', 'bcf_body_class' );


/* footer widget zone */

function bcf_register_sidebars() {
	register_sidebar( array(
		'id'            => 'footer-widgets-bottom',
		'name'          => __( 'Footer Bottom', 'promenade' ),
		'description'   => __( 'Bottom with black background.', 'promenade' ),
		'before_widget' => '<div id="contact" class="footer-widgets-bottom">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'bcf_register_sidebars' );

function promenade_credits() {
//	$text = 'Bord Cadre Films';
//	echo apply_filters( 'promenade_credits', $text );
	
	if ( is_active_sidebar( 'footer-widgets-bottom' ) ) :
	
					dynamic_sidebar( 'footer-widgets-bottom' );
	
	endif;
				
}



/* admin interface
******************************/

if ( is_user_logged_in() ) {
		require_once('functions-admin.php');
}


//
//
//function my_mem_settings() {
//	mem_plugin_settings( array( 'post' ), 'full' );
//}
//
//add_action( 'mem_init', 'my_mem_settings' );
//

<?php



function custom_admin_head() {
	//echo '<'get_stylesheet_directory() . "/admin/admin.css" ;
	//	echo "<link rel='stylesheet' id='admin-css'  href='".get_stylesheet_directory_uri()."/admin/admin.css' type='text/css' media='all' />";
	echo '<link rel="shortcut icon" href="'.get_stylesheet_directory_uri().'/img/favicon.ico?1313" />';
//	echo '<link rel="apple-touch-icon-precomposed" href="'.get_stylesheet_directory_uri().'/img/favicon-152.png" />';
}

add_action( 'admin_head', 'custom_admin_head' );


/* Dashboard improvement
******************************/

function tabula_remove_dashboard_widgets() {
	// Globalize the metaboxes array, this holds all the widgets for wp-admin
	global $wp_meta_boxes;
	
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );

	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'] );

	unset( $wp_meta_boxes['dashboard']['normal']['core']['jetpack_summary_widget'] );
	

	// RSS feeds:
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );

}
add_action( 'wp_dashboard_setup', 'tabula_remove_dashboard_widgets' );



/**
 * Show recent posts : 
 * https://gist.github.com/ms-studio/6069116
 */
 
function wps_recent_posts_dw() {
?>
   <ul class="dash-recent-posts" style="list-style-type: none;padding-left: 0;">
   		<style>
   			.dash-recent-posts .date {
   			}
   			.dash-recent-posts .separator {
   				padding: 0 0.4em;
   			}
   		</style>
     <?php
          global $post;
          $args = array( 
          	'posts_per_page' => 7,
          	'post_type' => array('post','page', 'news')
          );
          $myposts = get_posts( $args );
                foreach( $myposts as $post ) :  setup_postdata($post); 
                		
                		$post_edit_link = admin_url().'post.php?post='.get_the_ID().'&action=edit' ;
                		$sprtr = '<span class="separator">–</span>';
                ?>
                    <li>
                    <? the_date('j F','',''); echo $sprtr; ?>
                    <b><a href="<?php echo $post_edit_link; ?>"><?php the_title(); ?></a></b> 
                    <?php echo $sprtr; ?><span class=""><a href="<?php echo $post_edit_link; ?>">modifier</a> • 
                    <a href="<?php the_permalink(); ?>">voir</a></span></li>
          <?php endforeach; ?>
   </ul>
<?php
}
function add_wps_recent_posts_dw() {
       wp_add_dashboard_widget( 'wps_recent_posts_dw', __( 'Recent Posts' ), 'wps_recent_posts_dw' );
}
add_action('wp_dashboard_setup', 'add_wps_recent_posts_dw' );



// source: https://snipt.net/dnnsldr/add-a-custom-meta-box-to-wordpress-dashboard/
//adds a custom meta box in the dashboard of WordPress
//this one is for a simple "Contact Your Web Designer" but can be anything

//add to functions.php file in WP
//add an metabox for support into the dashboard and remove default meta boxes
    
function custom_dashboard_help() {
    ?>
    <ul>
    	<li><a href="<?php echo admin_url(); ?>post-new.php" class="button-primary">Ajouter un Film</a></li>
    	<li><a href="<?php echo admin_url(); ?>edit.php" class="button-secondary">Gérer les Films</a></li>
    	
    	<li><a href="<?php echo admin_url(); ?>post-new.php?post_type=page" class="button-primary">Ajouter une Page</a></li>
    	<li><a href="<?php echo admin_url(); ?>edit.php?post_type=page" class="button-secondary">Gérer les Pages</a></li>
    	
    	<hr class="custom-dash-divider" />
    	<li><a href="/documentation/" class="site-documentation" target="_blank">Documentation Site Bord Cadre</a></li>
    	<li><a href="mailto:ms@ms-studio.net?subject=Site%20Bord%20Cadre" class="site-documentation site-contact" target="_blank">Contact Webmaster</a></li>
    	
    </ul>
    <style>
    	.custom-dash-divider {
    		margin-top: 1em;
    	}
    	#dashboard-widgets .site-documentation:before {
    			font: 400 20px/1 dashicons;
    			content: "\f348";
    			color: #aaa;
    			speak: none;
    			display: block;
    			float: left;
    			margin: 0 5px 0 0;
    			padding: 0;
    			text-indent: 0;
    			text-align: center;
    			position: relative;
    			-webkit-font-smoothing: antialiased;
    			text-decoration: none!important;
    	}
    	#dashboard-widgets .site-contact:before {
    		content: "\f465";
    	}
    </style>
    <?php
}
function mycustom_dashboard_widgets() {
    global $wp_meta_boxes;
		wp_add_dashboard_widget('custom_help_widget', 'Actions fréquentes', 'custom_dashboard_help');
}
add_action('wp_dashboard_setup', 'mycustom_dashboard_widgets');   



<?php
/**
 * The template part for displaying content for individual posts.
 *
 * @package Promenade
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>'); ?>

		<?php if ( 'news' == get_post_type() ) : ?>

			<p class="entry-meta entry-meta--header">
				<?php // promenade_posted_by(); ?>

				<?php promenade_entry_date( true ); ?>
			</p>

		<?php endif; ?>
	</header>

	<div class="entry-content" itemprop="text">
		<?php 
		
		if ( has_post_thumbnail() ) {
		
			echo '<div class="director-thumbnail">';
			the_post_thumbnail( 'medium' );
			echo '</div>';
		
//			if ( 'director' == get_post_type() ) {
//			
//					// show image
//				
//			}
		
		}
		
		the_content(); ?>
		
		<?php 
		
		if ( 'director' == get_post_type() ) {
		
				// show filmography
		
				if (get_field('bcf_filmographie'))
				{
					?><div class="filmographie">
					<h3><?php _e( 'Filmography', 'promenade' ); ?></h3>
						<?php the_field('bcf_filmographie'); ?>
					</div>
					<?php
				}		
		
		}
		
		
		 ?>

		<?php promenade_page_links(); ?>
	</div>

	<footer class="entry-meta entry-meta--footer">
		<?php promenade_post_terms(); ?>

		<?php edit_post_link( __( 'Edit', 'promenade' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
</article>

<?php
/**
 * The template used for displaying content in page.php.
 *
 * @package Promenade
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/CreativeWork">
	<header class="entry-header">
		<div class="page-fence">
			<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>'); ?>
		</div>
	</header>

	<div class="entry-content" itemprop="text">
		<?php 
		
				if ( has_post_thumbnail() ) {
				
					echo '<div class="director-thumbnail">';
					the_post_thumbnail( 'medium' );
					echo '</div>';
				
				}
		
		the_content(); ?>

		<?php promenade_page_links(); ?>
	</div>

	<?php edit_post_link( __( 'Edit', 'promenade' ), '<footer class="entry-meta entry-meta--footer"><span class="edit-link">', '</span></footer>' ); ?>
</article>

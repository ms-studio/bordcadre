<?php
/**
 * The template part for displaying content for individual posts.
 *
 * @package Promenade
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("film-layout"); ?> itemscope itemtype="http://schema.org/Movie">
	
	<?php 
	
	/*
	 * Group 1:
	 **************
	 * 1. Titre
	 * 2. Citation
	 * 3. Logo Festival (mobile)
	 * 4. Trailer
	 * 5. Synopsis (mobile)
	 * 6. Button
	 * 7. Links (desktop)
	*/
	
	 ?>
	
	<div class="group group-1">
	
	<header class="entry-header film-title">
		<?php the_title( '<h1 class="entry-title" itemprop="name">', '</h1>'); ?>
	</header>
	
	<?php 
	
	// 2. Tagline
	
	if (get_field('film_tagline'))
	{
		
		echo '<div class="film-tagline">' . get_field('film_tagline') . '</div>';
	}
	
	// 3. Logo Festival
	
	// image file.
	// film_logo_festival, type: image
	
		$film_logo_fest = get_field('film_logo_festival');
				$film_logo_fest_output = '';
		//		echo '<pre>';
		//		var_dump($logo_festival);
		//		echo '</pre>';
						
				if( !empty($film_logo_fest) ): 
				
					// vars
					$url = $film_logo_fest['url'];
					$title = $film_logo_fest['title'];
					$alt = $film_logo_fest['alt'];
					$caption = $film_logo_fest['caption'];
				
					// thumbnail
					$size = 'large';
					$thumb = $film_logo_fest['sizes'][ $size ];
					$width = $film_logo_fest['sizes'][ $size . '-width' ];
					$height = $film_logo_fest['sizes'][ $size . '-height' ];
					
					$film_logo_fest_output = '<img src="'.$thumb.'" alt="'.$alt.'" width="'.$width.'" height="'.$height.'" />';
					
					 ?>
						<div class="logo-festival mobile-view">
							<?php echo $film_logo_fest_output; ?>
						</div>
				<?php endif;
	
	// 4. Trailer
	
	if (get_field('film_trailer'))
	{
		echo '<div class="embed-container film-trailer" itemprop="trailer">';
			the_field('film_trailer');
		echo '</div>';
	}
	
	// 5. Synopsis
	
	echo '<div class="entry-content film-synopsis body-text" itemprop="description">';
	echo '<h3>Synopsis</h3>';
		the_content();
	echo '</div>';
	
	// 6. "Watch Now!" button
	
	if (get_field('lien_vod'))
	{
		echo '<div class="watch-now">';
			the_field('lien_vod');
		echo '</div>';
	}
	
	// 7. Links (desktop)
	
	if (get_field('film_links'))
	{
		?>
		<div class="film-links desktop-view">
			<h3><?php _e( 'Links', 'promenade' ); ?></h3> 
			<?php the_field('film_links'); ?>
		</div>
		<?php
	}
	
	?>
	
	</div><!-- // end of group-1 -->
	
	<div class="group group-2">
	
	<?php 
		
	/*
	 * Group 2:
	 **************
	 * 1. Affiche
	 * 2. News (desktop)
	 * 3. Gallerie de photos
	 * 4. Downloads (desktop)
	*/
			
	// 1. Affiche
	// = Image à la une
	
	// itemprop="image"
	
	if ( has_post_thumbnail() ) {
	
		$affiche_id = get_post_thumbnail_id();
		
		// http://codex.wordpress.org/Gallery_Shortcode
		
		$affiche_shortcode = '[gallery ids="' . $affiche_id . '" size="medium" link="file"]';
		
		echo do_shortcode( $affiche_shortcode );
		
//		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
//		echo '<a href="' . $large_image_url[0] . '" title="' . the_title_attribute( 'echo=0' ) . '" itemprop="image">';
//		the_post_thumbnail( 'large' );
//		echo '</a>';
	}
	
	
	// 2. News
	
	$bcf_related_news = '';
	
	// tester si ce film est dans une catégorie qui est sous-catégorie de "films" (1)
	
	$categories = get_the_category();

	if($categories){
		foreach($categories as $category) {
			if ( $category->parent == 1 ) {
				
				// this category is child of "films"
				
				// query for posts!
				$custom_query = new WP_Query( array(
				  					 	'posts_per_page' => 5,
				  					 	'post_type' => 'news',
				  					 	'category_name' => $category->slug,
				  					 		'orderby' => 'date',
				  					 		'order' => 'DESC',
				  					 	) );
				  					 	
				  			if ($custom_query->have_posts()) : 
				  			 
				  				 	?>
				  				 	<div class="related-news smaller-text desktop-view">
				  				 	<h3><?php  _e( 'News', 'promenade' ); ?></h3>
				  				 	<?php
				  				 	
				  				 while( $custom_query->have_posts() ) : $custom_query->the_post();
				  				 		
				  				 		$bcf_related_news .= '<h4 class="h4">'.get_the_title().'</h4>';
				  				 		$bcf_related_news .=  '<div class="news-date">'.get_the_time("j F Y").'</div>';
				  				 		$bcf_related_news .= '<div class="content">';
				  				 		
				  				 		$bcf_related_news .=	apply_filters('the_content', get_the_content());
				  				 		$bcf_related_news .= '</div>';
				  				
				  				endwhile; 
				  				
				  				echo $bcf_related_news;
				  					
				   			 ?></div><?php
				  			endif;
				  	wp_reset_postdata();	 		 	
				
				// end of custom query
				
			}
		}
	}
	
	
	// 3. Gallerie: film_galerie
	
	$image_ids = get_field('film_galerie', false, false);
	
	if ( !empty($image_ids) ) {
	
		$gallery_shortcode = '[gallery ids="' . implode(',', $image_ids) . '" size="medium" link="file"]';
	
		echo do_shortcode( $gallery_shortcode );
	
	}
	
	// 4. Downloads (desktop)
	
	$bcf_downloads = get_field('film_file_downloads');
		 
		    if ( !empty( $bcf_downloads ) ) {
		    
		    ?>
		    <div class="film-files desktop body-text">
		    
		    	<h3><?php  _e( 'Downloads', 'promenade' ); ?></h3> 
		    
		    	<ul>
		    	<?php
		    	
		    		foreach($bcf_downloads as $row) {
		    		
	//				    echo '<pre>The $row:';
	//				    var_dump($row);
	//				    echo '</pre>';
					    
					    echo '<li><a href="';
					    echo $row["film_fichier_joint"]["url"];
					    echo '" >';
					    echo $row["film_fichier_joint"]["title"];
					    echo '</a></li>';
					    
				    }
						    
						?>
						</ul>
						</div>
						<?php
						
				}
	
	?>
	
	</div><!-- // end of group-2 -->
	
	<div class="group group-3">
	
	<?php 
	
	/*
	 * Group 3:
	 **************
	 * 1. Logo Festival (desktop)
	 * 2. Prix et festivals
	 * 3. Director
	 * 4. Cast + Crew
	 
	 * 5. Links (mobile)
	 * 6. Downloads (mobile)
	 * 7. News (mobile)
	*/
	
	
	// 1. Logo Festival (desktop)
	
		if( !empty($film_logo_fest_output) ): 
			 ?>
				<div class="logo-festival desktop-view">
					<?php echo $film_logo_fest_output; ?>
				</div>
		<?php endif;
	
	
	// 2. Prix + Festivals
	
	if (get_field('film_prix_festivals'))
	{
		?><div class="prix-festivals small-text" itemprop="award">
			<?php the_field('film_prix_festivals'); ?>
		</div>
		<?php
	}
	
	// 3. Director
	
	$director = get_field('film_realisateur');
	
//	echo '<pre>';
//	var_dump($director);
//	echo '</pre>';
if( $director ) {
	
	$director_id = $director->ID;
	
	// echo $director_id;

	$custom_query = new WP_Query( array(
	      					'post_type' => 'director',
	      					'page_id' => $director_id
	      			) ); 
	      			
	      			if ($custom_query->have_posts()) : 
	      		?>
	      		<div class="film-director body-text" itemprop="director" itemscope itemtype="http://schema.org/Person">    		          			
	      		<?php 
	      		while( $custom_query->have_posts() ) : $custom_query->the_post();
	      				
	      				?><h3><?php 
	      				
	      				_e( 'Director', 'promenade' );
	      				
	      				// echo $director->post_title; 
	      				
	      				?></h3>
	      				<div itemprop="description">
	      				<?php 
	      					
	      					echo '<a href="'; 
	      					the_permalink(); 
	      					echo '">';
	      					the_post_thumbnail( 'thumbnail' ); 
	      					echo '</a>';
	      				 
	      				 //echo $director->post_content;
	      				 
	      				 ?><h3 class="director-name" itemprop="name"><a href="<?php the_permalink(); ?>">
	      				 <?php the_title();  ?>
	      				 </a></h3>
	      				 <?php
	      				
	      					// the_excerpt();
	      					// the_content( "&rarr; Bio" , true );
	      				
	      				?></div>
	      				<?php	
	      				
	      				// get custom field bcf_filmographie
	      				
	      				if (get_field('bcf_filmographie'))
	      				{
	      					?><div class="filmographie small-text">
	      					<h3><?php _e( 'Filmography', 'promenade' ); ?></h3>
	      						<?php the_field('bcf_filmographie'); ?>
	      					</div>
	      					<?php
	      				}
	
		       	endwhile; ?>
	      		 </div>
	      		 <?php
	      		 
	      		 endif;
	     wp_reset_postdata();
	
	} // if director
	
	// 4. Cast & Crew
	
	if (get_field('film_cast_crew'))
	{
		?><div class="cast-crew small-text">
			<?php the_field('film_cast_crew'); ?>
		</div>
		<?php
	}
	
	// 5. Links
		
	if (get_field('film_links'))
	{
		?>
		<div class="film-links body-text mobile-only">
			<h3><?php _e( 'Links', 'promenade' ); ?></h3> 
		
			<?php the_field('film_links'); ?>
		</div>
		<?php
	}
	
	// 6. Downloads - film_downloads (mobile only)
	
	$acf_repeater_field = get_field('film_file_downloads');
	 
	 
	    if ( !empty( $acf_repeater_field ) ) {
	    
	    ?>
	    <div class="film-files body-text mobile-only">
	    
	    	<h3><?php _e( 'Downloads', 'promenade' ); ?></h3> 
	    
	    	<ul>
	    	<?php
	    	
	    		foreach($acf_repeater_field as $row) {
	    		
//				    echo '<pre>The $row:';
//				    var_dump($row);
//				    echo '</pre>';
				    
				    echo '<li><a href="';
				    echo $row["film_fichier_joint"]["url"];
				    echo '" >';
				    echo $row["film_fichier_joint"]["title"];
				    echo '</a></li>';
				    
			    }
					    
					?>
					</ul>
					</div>
					<?php
					
			}
	
	// 7. Related News (mobile)
	
			if (!empty($bcf_related_news)) {
				
				?>
					<div class="related-news small-text mobile-view">
					<h3><?php  _e( 'News', 'promenade' ); ?></h3>
					<?php
			
						echo $bcf_related_news;
				
				?></div><?php
			}
		
		?>
		
		</div><!-- // end of group-3 -->
		
		<?php 
		
	
	 ?>
	
</article>

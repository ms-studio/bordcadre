<?php
/**
 * The header for our theme.
 *
 * @package Promenade
 * @since 1.0.0
 */
 
 // test if english or french
 
 $bcf_lang_var = bcf_lang_tax();
   
?><!DOCTYPE html>
<html class="no-js" <?php 
		
		echo 'lang="'.$bcf_lang_var.'"';
		// language_attributes();

 ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php wp_head(); 
	
	$my_current_language = get_bloginfo('language');
	
//	 echo $my_current_language;
		// echo $_SERVER['REQUEST_URI'];

	?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'promenade' ); ?></a>

		<?php do_action( 'promenade_before' ); ?>

		<header id="masthead" class="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
			<?php promenade_site_title(); ?>

			<nav class="site-navigation clearfix" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
				<button class="site-navigation-toggle"><?php _e( 'Menu', 'promenade' ); ?></button>
				<ul id="menu-navigation-principale" class="menu">
				<?php
				
				if ( $bcf_lang_var == "en" ) {
					$bcf_menu_name = 'primary_en';
				} else {
					$bcf_menu_name = 'primary';
				}
				
				wp_nav_menu( array(
					'theme_location' => $bcf_menu_name,
					'container'      => false,
					'menu_class'     => 'menu',
					'fallback_cb'    => 'promenade_primary_nav_menu_fallback_cb',
					'depth'          => 3,
					'items_wrap'     => '%3$s',
				) );
				
				
				// English / French 
				
				include_once( 'functions-lang.php' );
								
				?>
				
				</ul>
			</nav>
		</header>

		<?php do_action( 'promenade_content_before' ); ?>

		<div id="content" class="site-content">

			<?php do_action( 'promenade_content_top' ); ?>

			<div class="site-content-inside">
				<div class="page-fence">

					<?php do_action( 'promenade_content_inside_top' ); ?>
